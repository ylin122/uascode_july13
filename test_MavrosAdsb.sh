gnome-terminal \
	--tab --title "roscore" --command "bash -c \"
                                roscore;
				exec bash\"" \
	--tab --title "mavros" --command "bash -c \"
				env sleep 3s;
				roslaunch mavros px4.launch fcu_url:="/dev/ttyUSB0:57600" gcs_url:="udp://:14555@192.168.0.2:14550"  mission/pull_after_gcs:="true"
				exec bash\"" \
	--tab --title "MavrosListen" --command "bash -c \"
				env sleep 3s;
				#rosrun uascode test_MavrosListen wp_r home_alt
				rosrun uascode test_MavrosListen 25 1406.10
				exec bash\"" \
        --tab --title "ScaleAdsbFromFile" --command "bash -c \"
				env sleep 3s;
				rosrun uascode test_adsbscale LoadSendConfig.txt 3
				exec bash\"" \
