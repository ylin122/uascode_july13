#include "MavlinkRecNode.hpp"
#include "common/Utils/YcLogger.h"

using namespace UasCode;

int main(int argc, char** argv)
{
    ros::init(argc,argv,"MavRecNode");

    Utils::LogConfigurator myconfigurator("log4cxx_MavRecNode.properties", "log for MavRecNode");

    MavlinkRecNode mavrec_node;
    mavrec_node.TcpSetUp();
    mavrec_node.working();
}
