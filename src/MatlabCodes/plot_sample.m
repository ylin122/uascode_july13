close all;
clear all;
clc;

f_traj = fopen('/home/yucong/catkin_ws/devel/lib/uascode/sample.txt','r');

if f_traj == -1
    error('File samples.txt could not be opened, check name or path.')
end

line1 = fgetl(f_traj);
set_points = [];

while ischar(line1)
    log_traj = textscan(line1,'%f %f %f');
    x = log_traj{1};
    y = log_traj{2};
    set_points = [ set_points; [x,y] ];
    line1= fgetl(f_traj);
end

figure
axis equal;
%axis([-300 300 0 800])
plot( set_points(:,1), set_points(:,2), 'r+' );
%line( [100,100+50*cosd(30)], [34,34+50*sind(30)] ); 
hold on;