#pragma once
#include "UserStructs/obstacle3D.h"

#include <fstream>

#include "ros/ros.h"
//ros messages
#include "yucong_rosmsg/MultiObsMsg2.h"
#include "yucong_rosmsg/ObsMsg2.h"
#include "std_msgs/UInt16.h"
#include "mavros/State.h"

namespace UasCode{

class ScaleAdsbFromFile{
public:
   ScaleAdsbFromFile();
   ~ScaleAdsbFromFile();
   //read adsbs
   void ReadADSB(const std::vector<std::string>& file_names);
   //send adsbs from file
   void SendObss2(bool f0, bool f1, bool f2);
   //load offsets from files
   void LoadOffsetsSingle(const char *filename,int idx);
   void LoadOffsets2(const char* off1,const char* off2,const char* off3,const char* type = "");
   void LoadSendConfig(const char *filename,const std::vector<std::string> &file_names);
   void LoadSendRandom(const std::vector<std::string> &file_names,const char* type="");
   void LoadSendRandomNum(const std::vector<std::string> &file_names,int num,const char* type="");

private:
   struct OffSet{
       double x_off;
       double y_off;
       double z_off;
       double hd_off;

       OffSet():x_off(0),y_off(0),z_off(0),hd_off(0){}
   };

   std::vector<OffSet> offsets;
   std::vector<std::vector<UserStructs::obstacle3D> > all_obss;
   int seq_current;
   std::string UavState;
   bool if_send_obstacle;
   bool if_mission;

   //ros related
   ros::NodeHandle nh;
   ros::Publisher pub_obss;
   ros::Subscriber sub_wp_curr;
   ros::Subscriber sub_state;

   //callback functions
   void mission_currentCb(const std_msgs::UInt16::ConstPtr &msg);
   void stateCb(const mavros::State::ConstPtr &msg);

   //other functions
   yucong_rosmsg::ObsMsg2 ObsToMsg2( const UserStructs::obstacle3D& obs );

   int RandSelect(int start,int end);
   int RandSelectVec(const std::vector<int>& ints);
   std::string int2string(int _num);
};

}
