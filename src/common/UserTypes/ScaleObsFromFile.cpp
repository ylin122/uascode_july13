#include "ScaleObsFromFile.hpp"
#include <stdexcept>
#include <fstream>
#include <map>
#include <vector>
#include "Planner/UserStructs/obstacle3D.h"
#include "common/Utils/GetTimeNow.h"
#include "common/Utils/YcLogger.h"
#include "common/UserStructs/constants.h"
#include "common/Utils/GeoUtils.h"
#include "common/Utils/FindPath.h"
#include "common/Utils/UTMtransform.h"
//ros msgs
#include "yucong_rosmsg/MultiObsMsg2.h"
#include "yucong_rosmsg/ObsMsg2.h"

namespace {
    Utils::LoggerPtr s_logger(Utils::getLogger("uascode.ScaleObsFromFile.YcLogger"));
}

namespace UasCode{

    ScaleObsFromFile::ScaleObsFromFile():seq_current(-1),if_send_obstacle(false),if_mission(false)
    {
        pub_obss = nh.advertise<yucong_rosmsg::MultiObsMsg2>("/mavros/multi_obstacles",10);
        sub_wp_curr = nh.subscribe("/mavros/mission_current",10,&ScaleObsFromFile::mission_currentCb,this);
        sub_state = nh.subscribe("/mavros/state",10,&ScaleObsFromFile::stateCb,this);
        for(int i = 0; i != 3; ++i){
            this->offsets.push_back( OffSet() );
        }
    }

    ScaleObsFromFile::~ScaleObsFromFile(){}

    void ScaleObsFromFile::LoadOffsetsSingle(const char *filename,int idx)
    {
        std::ifstream file_offset(filename);
        std::vector<OffSet> offs;

        if(file_offset.is_open())
        {
            while(file_offset.good() )
            {
               OffSet off_set;
               file_offset >> off_set.x_off
                       >> off_set.y_off
                       >> off_set.z_off
                       >> off_set.hd_off;
               offs.push_back(off_set);
            }
            this->offsets[idx] = offs[idx];
        }
        else{
            try{
                throw std::runtime_error("unable to load offsets file:"+ (std::string)filename);
            }
            catch (std::runtime_error &e) {
                std::cout << "Caught a runtime_error exception: " << e.what() << '\n';
            }
        }

    }

    void ScaleObsFromFile::LoadOffsets2(const char* off1, const char* off2, const char* off3, const char* type)
    {
        std::string filepath = Utils::FindPath();
        std::string file1 = filepath + "/recordsHIL/offsets_simu" + type + off1 + ".txt";
        std::string file2 = filepath + "/recordsHIL/offsets_simu" + type + off2 + ".txt";
        std::string file3 = filepath + "/recordsHIL/offsets_simu" + type + off3 + ".txt";

        UASLOG(s_logger,LL_DEBUG,file1 << "\n"
               << file2 << "\n"
               << file3 << "\n");

        this->LoadOffsetsSingle(file1.c_str(),0);
        this->LoadOffsetsSingle(file2.c_str(),1);
        this->LoadOffsetsSingle(file3.c_str(),2);
    }

    void ScaleObsFromFile::ReadObss(const char* filename)
    {
        std::vector<UserStructs::obstacle3D> obss;
        UserStructs::obstacle3D obs_single;

        UASLOG(s_logger,LL_INFO,"ReadObss starts");

        try
        {
          obss_file.open(filename);
        }
          catch (std::ifstream::failure& e) {
              UASLOG(s_logger,LL_WARN,"Exception opening/reading file "
                     << e.what());
        }

        while(obss_file.good() )
        {
            bool repeat= false;

            obss_file >> obs_single.address
                      >> obs_single.x1
                    >> obs_single.x2
                    >> obs_single.x3
                    >> obs_single.head_xy
                    >> obs_single.speed
                    >> obs_single.v_vert
                    >> obs_single.t
                    >> obs_single.r
                    >> obs_single.hr;

            obs_single.r = 300;
            obs_single.hr = 50.;
            obs_single.head_xy= obs_single.head_xy * UasCode::DEG2RAD;
            //insert into address map
            if(addrs_map.find(obs_single.address)== addrs_map.end())
            {
                int ss = addrs_map.size();
                addrs_map[obs_single.address] = ss;
                /*
                UASLOG(s_logger,LL_DEBUG,(int)obs_single.address
                       << " " << addrs_map[obs_single.address] );
                */
            }

            for(int i=0;i!= obss.size();++i)
            {
                if(obs_single.address== obss[i].address){
                    repeat= true;
                    break;
                }
            }

            if(repeat){
               for(int i=0;i!= obss.size();++i)
               {
                  int idx = addrs_map[ obss[i].address ];
                  obss[i].x1 += offsets[idx].x_off;
                  obss[i].x2 += offsets[idx].y_off;
                  obss[i].x3 += offsets[idx].z_off;
                  obss[i].head_xy += offsets[idx].hd_off;
                  /*
                  UASLOG(s_logger,LL_DEBUG,"obs_single x y:"
                                << std::setprecision(4)<< std::fixed
                                << (int)obss[i].address << " "
                                << obss[i].x1 << " "
                                << obss[i].x2);
                                */
               }
               all_obss.push_back(obss);
               obss.clear();
            }

            //push
            obss.push_back(obs_single);
        }//while ends
        UASLOG(s_logger,LL_INFO,"ReadObss endss");
        UASLOG(s_logger,LL_DEBUG,"all_obss size: " << all_obss.size() );
    }

    void ScaleObsFromFile::SendObss2(bool f1, bool f2, bool f3)
    {
        yucong_rosmsg::MultiObsMsg2 obss_msg;
        ros::Rate r(10);
        int count = 0;

        std::vector< uint32_t > vec_addrs;
        for (std::map<uint32_t,int>::iterator it=addrs_map.begin(); it!=addrs_map.end(); ++it)
        {
            vec_addrs.push_back(it->first);
        }

        bool if_start = false;

        while( ros::ok() )
        {
            ros::spinOnce();
            if( if_mission ){
                if_start = ( seq_current > 0 && UavState == "AUTO.MISSION");
            }

            if( if_start && count != all_obss.size() )
            {
                std::vector<UserStructs::obstacle3D> obss = all_obss[count];
                obss_msg.MultiObs.clear();

                bool if_pub = false;

                for( int j = 0; j != obss.size(); ++j )
                {
                   if( obss[j].address == vec_addrs[0] && f1
                     || obss[j].address == vec_addrs[1] && f2
                     || obss[j].address == vec_addrs[2] && f3
                     )
                   {
                       obss[j].t = Utils::GetTimeNow();
                       obss_msg.MultiObs.push_back( ObsToMsg2( obss[j]) );
                       if_pub = true;
                   }
                }

                if( if_pub ){
                    pub_obss.publish( obss_msg );
                }

                ++ count;
                sleep(1);
            }

        }//while ros ends

    }

    void ScaleObsFromFile::LoadSendConfig(const char *cfg_file, const char *obs_file)
    {
       std::string file= Utils::FindPath()+"/recordsHIL/"+std::string(cfg_file);
       std::ifstream config_file(file.c_str());

       int count = 0;
       std::string off0,off1,off2;
       std::string type;
       bool if0, if1, if2;

       if(config_file.is_open()){
           std::string line;
           while(getline(config_file,line))
           {
               std::istringstream iss(line);
               if(count==0){
                  iss >> off0 >> off1 >> off2 >> type;
               }

               if(count==1){
                  iss >> if0 >> if1 >> if2;
               }
               ++count;
           }//while ends
       }//if ends

       this->LoadOffsets2(off0.c_str(),off1.c_str(),off2.c_str(),type.c_str());
       this->ReadObss(obs_file);
       this->SendObss2(if0,if1,if2);
    }

    void ScaleObsFromFile::LoadSendRandomNum(const char *obs_file, int num, const char *type)
    {
        const int arr[]={60,80,100,120,140,160,180,200,220,240,260,280,300};
        std::vector<int> vec (arr, arr + sizeof(arr) / sizeof(arr[0]) );

        bool if0,if1,if2;
        std::string off0="0",off1="0",off2="0";

        srand(time(NULL));
        if(num==1)
        {
            int idx = rand() % 3;
            if(idx==0){
                if0= true;
                if1= false;
                if2= false;
            }
            else if(idx==1){
                if0= false;
                if1= true;
                if2= false;
            }
            else{
                if0= false;
                if1= false;
                if2= true;
            }
            //
        }
        else if(num==2){
            int idx = rand() % 3;
            if(idx==0){
              if0= false;
              if1= true;
              if2= true;
            }
            else if(idx==1){
              if0= true;
              if1= false;
              if2= true;
            }
            else{
              if0= true;
              if1= true;
              if2= false;
            }
            //
        }
        else{
            if0= true;
            if1= true;
            if2= true;
        }

        srand(time(NULL));
        if(if0){
          int nf0= this->RandSelectVec(vec);
          off0 = this->int2string(nf0);
        }

        if(if1){
          int nf1= this->RandSelectVec(vec);
          off1 = this->int2string(nf1);
        }

        if(if2){
          int nf2= this->RandSelectVec(vec);
          off2 = this->int2string(nf2);
        }

        UASLOG(s_logger,LL_DEBUG,"random offsets:"<< off0 <<","<<off1<<","<<off2);

        this->LoadOffsets2(off0.c_str(),off1.c_str(),off2.c_str(),type);
        this->ReadObss(obs_file);
        this->SendObss2(if0,if1,if2);
    }

    yucong_rosmsg::ObsMsg2 ScaleObsFromFile::ObsToMsg2( const UserStructs::obstacle3D& obs )
    {
        yucong_rosmsg::ObsMsg2 obs_msg;
        obs_msg.address = obs.address;
        double lon, lat;

        Utils::FromUTM( obs.x1, obs.x2, lon, lat );
        obs_msg.lon = lon;
        obs_msg.lat = lat;
        obs_msg.x3 = obs.x3;
        obs_msg.head_xy = obs.head_xy;
        obs_msg.speed = obs.speed;
        obs_msg.v_vert = obs.v_vert;
        obs_msg.t = obs.t;
        UASLOG(s_logger,LL_DEBUG,"obs_msg.t"
              << std::setprecision(4)<< std::fixed
              << obs_msg.t << " "
              << obs_msg.lat << " "
              << obs_msg.lon << " "
              << obs_msg.x3 << " "
              << obs_msg.head_xy << " "
              << obs_msg.speed << " "
              << obs_msg.v_vert << " "
              << obs.x1 << " "
              << obs.x2);
        obs_msg.r = obs.r;
        obs_msg.hr = obs.hr;
        return obs_msg;
    }

    int ScaleObsFromFile::RandSelectVec(const std::vector<int> &ints)
    {
        /* initialize random seed: */
        //srand (time(NULL));
        /* to generate a random number */
        int len = ints.size();
        int num = rand() % len;
        int idx;

        if (num > len-1)
            idx=0;
        else{
            idx= ints[num];
        }
        return idx;
    }

    std::string ScaleObsFromFile::int2string(int _num)
    {
        std::stringstream ss;
        ss << _num;
        return ss.str();
    }

    void ScaleObsFromFile::mission_currentCb(const std_msgs::UInt16::ConstPtr &msg)
    {
        seq_current = (int)msg->data;
    }

    void ScaleObsFromFile::stateCb(const mavros::State::ConstPtr &msg)
    {
        UavState = msg->mode;
    }
}


