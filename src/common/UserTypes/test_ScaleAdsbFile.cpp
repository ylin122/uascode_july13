#include "ScaleAdsbFromFile.hpp"
#include "ros/ros.h"
#include "common/Utils/YcLogger.h"
#include "common/Utils/FindPath.h"
#include <string>

using namespace UasCode;

int main(int argc,char** argv)
{
    ros::init(argc,argv,"scale_adsb_from_file");
    Utils::LogConfigurator myconfigurator("log4cxx_AdsbFromFile.properties","AdsbFromFile");

    //rank from small to large
    std::string file_adsb0 = "scaled_obstacle_10734770.txt";
    std::string file_adsb1 = "scaled_obstacle_10934723.txt";
    std::string file_adsb2 = "scaled_obstacle_10942331.txt";

    std::vector<std::string> file_names;
    file_names.push_back(file_adsb0);
    file_names.push_back(file_adsb1);
    file_names.push_back(file_adsb2);

    ScaleAdsbFromFile adsbfrom;

    //adsbfrom.LoadSendConfig(argv[1],file_names);
    //adsbfrom.LoadSendRandom(file_names,"A");
    adsbfrom.LoadSendRandomNum(file_names,atoi(argv[2]) );
    return 0;
}
