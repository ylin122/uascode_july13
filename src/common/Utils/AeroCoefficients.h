#pragma once

namespace Utils{
  double CL_Lift(double angle_attack);
  double CD0_Drag0Lift(double angle_attack);
  double K_InducedDrag();
}
