#include "NavigatorSim.hpp"
#include "common/UserStructs/constants.h"
#include "common/Utils/GetTimeUTC.h"
#include "common/Utils/FindPath.h"
#include "Planner/UserStructs/PlaneStateSim.h"
//other
#include "armadillo"
#include <fstream>

using namespace UasCode;

int main(int argc, char** argv)
{
    NavigatorSim navigator("fs_action.txt","fs_state.txt");
    //parameters
    //double _Tmax= 12.49*CONSTANT_G;
    double _Tmax= 6.79*CONSTANT_G;
    //double _Muav= 29.2; //kg
    double _Muav= 0.453592*13;
    double myaw_rate= 20./180*M_PI;
    double mpitch_rate= 10./180*M_PI;
    double _max_speed= 15; //m/s
    double _min_speed= 10; //m/s
    double _max_pitch= 25./180*M_PI;
    double _min_pitch= -20./180*M_PI;

    double dt= 1.0;
    double _speed_trim = _max_speed;

    int des_id = atoi( argv[1] );

    navigator.UpdaterSetParams(_Tmax,mpitch_rate,myaw_rate,_Muav,
        _max_speed,_min_speed,_max_pitch,_min_pitch);

    std::string param_file = Utils::FindPath()+"parameters/parameters_sitl.txt";
    navigator.TECsReadParams(param_file.c_str());
    navigator.L1SetRollLim(40./180*M_PI);
    navigator.SetDt(dt);
    navigator.SetSpeedTrim(_speed_trim);

    /****read start state****/
    std::ifstream start_file( (Utils::FindPath()+"/recordsHIL/traj_log_compare.txt").c_str() );
    double t_now,lon_f,lat_f,hgt_f,speed_f,x_f,y_f,pitch_f,yaw_f,ax_f,ay_f,az_f;
    int wp_id = -1;

    std::string line;
    while(std::getline(start_file,line)){
        std::istringstream iss(line);
        iss >> wp_id >> t_now
                >> lat_f
                >> lon_f
                >> x_f >> y_f >> hgt_f
                >> speed_f
                >> yaw_f
                >> pitch_f
                >> ax_f >> ay_f >> az_f;
        if(wp_id == des_id) break;
    }
    /****read end state****/
    std::cout << "read traj finished" << '\n';
    /****read all waypoints****/
    std::vector< arma::vec::fixed <3> > wps;
    std::ifstream wp_file("/home/yucong/catkin_ws/src/uascode/recordsHIL/waypoints2.txt");
    int line_count = 0;
    if( wp_file.is_open() )
    {
        std::string line;
        while( getline( wp_file, line ) )
        {
            if( line_count == 0 ){
                std::string str1, str2, str3;
                std::istringstream iss(line);
                iss >> str1 >> str2 >> str3;
            }
            else{
                std::istringstream iss(line);
                //0	1	0	16	0	25	0	0	33.4409317423789147	-111.995573043823242	1436.70000000000005	1
                int seq,frame,command,current,autocontinue;
                float param1,param2,param3,param4;
                double lat,lon,alt;
                iss >> seq >> current >> frame >> command
                        >> param1 >> param2 >> param3 >> param4
                        >> lat >> lon >> alt >> autocontinue;
                arma::vec::fixed< 3 > wp;
                wp << lat << lon << alt;
                wps.push_back( wp );
            }
            ++ line_count;
        }
    }
    /****real all waypoints ends****/
    std::cout << "read waypoints finished" << '\n';
    //test
    arma::vec::fixed< 2 > pt_A, pt_B;
    double hA, hB;
    arma::vec::fixed< 3 > wpp = wps[ des_id - 1 ];
    pt_A << wpp(0) << wpp(1);
    hA = wpp(2);
    wpp = wps[ des_id] ;
    pt_B << wpp(0)<< wpp(1);
    hB = wpp(2);

    double lat = lat_f;
    double lon = lon_f;
    double hgt = hgt_f;
    double spd = speed_f;
    double t = t_now;
    double yaw = yaw_f;
    double pitch = pitch_f;
    double ax = ax_f ;
    double ay = ay_f ;
    double az = az_f ;

    UserStructs::PlaneStateSim st(t,0,0,lat,lon,hgt,spd,yaw,pitch,ax,ay,az);
    //target mission point
    UserStructs::MissionSimPt Pt( pt_B(0), pt_B(1), hB, yaw,100, 0, 0, 400, 300, 30 );

    //future state
    UserStructs::PlaneStateSim st_end;
    //go to the waypoint: between two normal waypoints
    int result = navigator.PropagateWp(st,st_end,pt_A,Pt);
}
