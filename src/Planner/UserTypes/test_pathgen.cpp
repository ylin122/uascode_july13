//test for PathGenerator.cpp
#include "PathGenerator.hpp"
//USR TYPES
#include "Planner/UserStructs/SpaceLimit.h"
#include "Planner/UserStructs/PlaneStateSim.h"
#include "Planner/UserStructs/MissionSimPt.h"
#include "Planner/UserTypes/Sampler/SamplerPole.hpp"
#include "Planner/UserTypes/ObsHelper.hpp"
#include "common/UserStructs/constants.h"
//utils
#include "common/Utils/UTMtransform.h"
#include "common/Utils/GetTimeNow.h"
#include "common/Utils/FindPath.h"
#include "common/Utils/YcLogger.h"
//std
#include <iostream>
#include <fstream>

int main(int argc, char** argv)
{
  Utils::LogConfigurator myconfigurator("log4cxx_TestPathgen.properties","log for TestPathgen");

  std::ofstream fs_points("out_points.txt");
  std::ofstream fs_two("out_two.txt");
  //start a path generator
  UasCode::PathGenerator path_gen;
  //spacelimit
  UserStructs::SpaceLimit spacelimit(2000,500);
  spacelimit.LoadGeoFence( (Utils::FindPath()+"parameters/geofence.txt").c_str() );
  path_gen.SetSpaceLimit(spacelimit); 
  path_gen.SetTimeLimit(0.1);
  //path_gen.SetNinter(5);
  //if in ros?
  path_gen.SetInRos(false);
  //init state

  //407384.0559 3700607.1274 1454.3899 33.4409 -111.9964 14.7502 -98.0554 3.9398

  double xs = 407384.0559;
  double ys = 3700607.1274;
  double hgt= 1454.3899;
  double spd= 14.7502;

  double t= 0;
  double yaw= ( 90.-(-98.0554) )/ 180 * M_PI;
  double pitch= 3.9398 / 180 * M_PI;

  UserStructs::PlaneStateSim st(t,xs,ys,0,0,hgt,spd,yaw,pitch,0,0,0);
  st.GetGCS();

  std::cout<<"start_x: "<< st.x<<" "
           <<"start_y: "<< st.y<<" "
	   <<"start_z: "<< st.z<<" "
	   << std::endl;
  fs_points<<st.x<<" "<<st.y<<" "<<st.z<<" "<<std::endl;

  //start waypoint
  //33.4409 -111.996 1436.7 407461.0484 3700613.6590
  double lat1= 33.4409, lon1= -111.996, alt_A = 1436.7;
  UserStructs::MissionSimPt pt( lat1, lon1, alt_A, 0, 100, 0, 0, 200, 250, 150 );
  pt.GetUTM();

  //set start state and goal waypoint
  path_gen.SetInitState(st);
  path_gen.SetStartWp(pt);

  //goal waypoint
  //33.441 -112.029 1436.7 404342.1329 3700651.6447
  double lat2 = 33.441, lon2 = -112.029, alt_B = 1436.7;
  UserStructs::MissionSimPt pt1(lat2,lon2,alt_B,0,100,0,0,200,250,150);
  pt1.GetUTM();
  path_gen.SetGoalWp(pt1);

  path_gen.SetSampleStart(st.x,st.y,st.z);
  //path_gen.SetPlot( true );

  //parameters for the navigator
  //parameters
  double _Tmax= 6.79*UasCode::CONSTANT_G;
  double _Muav= 0.453592*13;
  double myaw_rate= 20./180*M_PI;
  double mpitch_rate= 10./180*M_PI;
  double _max_speed= 30.8667; //m/s
  double _min_speed= 10; //m/s
  double _max_pitch= 25./180*M_PI;
  double _min_pitch= -20./180*M_PI;
  
  double dt= 1.;
  double _speed_trim= _max_speed;
  //set
  path_gen.NavUpdaterParams(_Tmax,mpitch_rate,myaw_rate,_Muav,_max_speed,_min_speed,_max_pitch,_min_pitch);

  path_gen.NavTecsReadParams((Utils::FindPath()+"parameters/parameters_sitl.txt").c_str());
  path_gen.NavL1SetRollLim(10./180*M_PI);
  path_gen.NavSetDt(dt);
  path_gen.NavSetSpeedTrim(_speed_trim);
  //set sampler parameters
  path_gen.SetSampler(new UserTypes::SamplerPole() );
  path_gen.SetSampleParas();

  //set obstacles
  // 406148.3834 3700629.7028 1439.0161 15.4333 0.0000 0.0000

  double xo= 406148.3834,yo= 3700629.7028,zo= 1439.0161;

  double head_xy= 0;
  double spd1= 15.433;
  double vv= 0;
  double t1= t;
  double r= 300;
  double hr= 50;
  double dr=0, dhr=0;
  UserStructs::obstacle3D obs3d(11,xo,yo,head_xy,spd1,zo,vv,t1,r,dr,hr,dhr);
  std::vector<UserStructs::obstacle3D> v_obs3d;
  v_obs3d.push_back(obs3d);
  path_gen.SetObs(v_obs3d);

  std::vector< UasCode::ObsHelper >* helpers = new std::vector< UasCode::ObsHelper >();
  for(int i = 0; i!= v_obs3d.size(); ++i){
      helpers -> push_back( UasCode::ObsHelper( v_obs3d[i], dt ) );
  }
  //set helpers
  path_gen.NavSetHelpers(helpers);
  path_gen.NavSetIfSet( false );

  double tc1= Utils::GetTimeNow();
  //add paths
  std::cout << "first num inter wps: "
            << path_gen.AddPaths2() << std::endl;
  //std::cout << "third num inter wps: "
  //          << path_gen.AddPaths3() << std::endl;
  //st_current
  UserStructs::PlaneStateSim st_current= st;
  //
  UserStructs::MissionSimPt inter_wp;
  if(path_gen.PathCheckRepeat(st_current)){
    std::cout<<"check ok" << std::endl;
    inter_wp= path_gen.GetInterWp();
    std::cout<<"inter_x: "<< inter_wp.x<<" "
             <<"inter_y: "<< inter_wp.y<<" "
	     <<"inter_z: "<< inter_wp.alt<< std::endl;
    
    fs_points<<inter_wp.x<<" "<<inter_wp.y<<" "
      <<inter_wp.alt<< std::endl;
    
    UserStructs::PlaneStateSim st_inter= path_gen.GetInterState();
    fs_two<< st_inter.t<<" "
          << st_inter.x<<" "
	  << std::setprecision(10)<< st_inter.y<<" "
	  << st_inter.lat<<" "
	  << st_inter.lon<<" "
	  << st_inter.z<<" "
	  << st_inter.speed<<" "
	  << st_inter.yaw<<" "
	  << st_inter.pitch<<" "
	  << st_inter.ax<<" "
	  << st_inter.ay<<" "
	  << st_inter.az<<" "
	  << std::endl;

    fs_two<< pt.lat<<" "
          << pt.lon<<" "
          << pt.alt<<" "
          << pt.yaw<<" "
          << pt.r<<" "
	  << pt.x<<" "
	  << pt.y<<" "
	  << pt.h_rec<<" "
	  << pt.v_rec<<" "
	  << pt.alt_rec
	  << std::endl;
     
    path_gen.PrintPath("path_log.txt");
  }
  std::cout<<"time used: "<< Utils::GetTimeNow()-tc1<< std::endl;
  return 0;
}
