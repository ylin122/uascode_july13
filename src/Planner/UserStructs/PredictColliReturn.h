#pragma once
#include <vector>
namespace UserStructs{

struct PredictColliReturn{
    bool colli;
    int seq_colli; //predicted collision happend between (seq_colli)th and (seq_colli+1)th waypoints
    double time_colli;//time of predicted collision from now on
    double x_colli;
    double y_colli;
    double z_colli;

    int obs_id;
    std::vector< int > obs_ids;
    //constructor
    PredictColliReturn();
};


}
